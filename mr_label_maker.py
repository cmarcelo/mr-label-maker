#!/usr/bin/env python3


import sys
from mr_label_maker import mr_label_maker

if __name__ == '__main__':
    mr_label_maker.main(sys.argv[1:])