#!/usr/bin/env python3

# Copyright © 2023 Red Hat, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import mr_label_maker.mr_label_maker as mlm

from unittest.mock import patch, MagicMock
import pytest


@pytest.fixture
def token() -> str:
    return "topsecret"


# Mesa is the only project we have atm, so we patch that
# whole module. This way none of our tests actually go
# through to the project and we can't accidentally trigger
# real stuff happening.
#
# But it *does* mean we can easily test what the CLI does
# to the backend project - by assigning a MagicMock
# to mesa.Mesa.return_value we can then test that mock
# for all the calls that would've otherwise gone into
# the project itself. Which means we can test the CLI
# without having to care about how (or whether) the
# backend itself works.
@patch("mr_label_maker.mr_label_maker.mesa")
class TestCli:
    def test_invalid_project(self, mesa):
        with pytest.raises(SystemExit) as excinfo:
            mlm.main(['--project', 'foobar'])
        # argparse calls sys.exit(2) for
        # invalid arguments. That's the same as our API_ERROR
        assert excinfo.value.code == mlm.ExitCode.API_ERROR

    def test_missing_token(self, mesa):
        exit_code = mlm.main(['--project', 'mesa'])
        assert exit_code == mlm.ExitCode.USER_ERROR

    @pytest.mark.parametrize("dry_run", (True, False))
    @pytest.mark.parametrize("label", (True, False))
    @pytest.mark.parametrize("state", [None, 'opened', 'closed', 'merged', 'all'])
    @pytest.mark.parametrize("ignore_history", (True, False))
    def test_pass_args(self, mesa, token, dry_run, label, state, ignore_history):
        proj = MagicMock()
        mesa.Mesa.return_value = proj

        # Fixed args because otherwise we don't get anywhere
        args = [
            '--project', 'mesa',
            '--token', token,
        ]
        if dry_run:
            args.append('--dry-run')
        if label:
            args.extend(['--label', 'foolabel'])
        if state:
            args.extend(['--state', state])
        if ignore_history:
            args.append('--ignore-label-history')

        exit_code = mlm.main(args)
        assert exit_code == mlm.ExitCode.SUCCESS

        # Not let's make sure we passed everything through as expected
        proj.set_token.assert_called_with(token)
        proj.set_dry_run.assert_called_with(dry_run)
        proj.set_label.assert_called_with(['foolabel'] if label else [])
        proj.set_state.assert_called_with(state or 'opened')
        proj.set_ignore_label_history.assert_called_with(ignore_history)

        proj.connect.assert_called()

    @pytest.mark.parametrize("arg_name", ["--issues", "-i"])
    @pytest.mark.parametrize("arg", ["skip", "specific", "no-arg"])
    def test_pass_issues(self, mesa, token, arg_name, arg):
        proj = MagicMock()
        mesa.Mesa.return_value = proj

        # Fixed args because otherwise we don't get anywhere
        args = [
            '--project', 'mesa',
            '--token', token,
        ]

        if arg == "skip":
            pass
        elif arg == "specific":
            args += [arg_name, "123"]
        elif arg == "no-arg":
            args += [arg_name]

        exit_code = mlm.main(args)
        assert exit_code == mlm.ExitCode.SUCCESS

        proj.connect.assert_called()
        if arg == "skip":
            proj.process_issues.assert_not_called()
        elif arg == "specific":
            proj.process_issues.assert_called_with(123)
        elif arg == "no-arg":
            proj.process_issues.assert_called_with(-1)

    @pytest.mark.parametrize("arg_name", ["--merge-requests", "-m", "--mrs"])
    @pytest.mark.parametrize("arg", ["skip", "specific", "no-arg"])
    def test_pass_merge_request(self, mesa, token, arg_name, arg):
        proj = MagicMock()
        mesa.Mesa.return_value = proj

        # Fixed args because otherwise we don't get anywhere
        args = [
            '--project', 'mesa',
            '--token', token,
        ]

        if arg == "skip":
            pass
        elif arg == "specific":
            args += [arg_name, "123"]
        elif arg == "no-arg":
            args += [arg_name]

        exit_code = mlm.main(args)
        assert exit_code == mlm.ExitCode.SUCCESS

        proj.connect.assert_called()
        if arg == "skip":
            proj.process_mrs.assert_not_called()
        elif arg == "specific":
            proj.process_mrs.assert_called_with(123)
        elif arg == "no-arg":
            proj.process_mrs.assert_called_with(-1)